﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CSV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSV.Tests
{
    [TestClass()]
    public class CSVColumnInfoTests
    {
        [TestMethod()]
        public void CSVColumnInfoTest()
        {
            CSVColumnInfo column_info = new CSVColumnInfo( "ColumnName", 666 );

            Assert.IsTrue( column_info.Name == "ColumnName" );
            Assert.IsTrue( column_info.Index == 666 );
        }

    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CSV;


namespace CSV.Tests
{
    [TestClass()]
    public class CSVCellInfoTests
    {
        [TestMethod()]
        public void CSVCellInfoTest()
        {
            CSVCellInfo cell = new CSVCellInfo( new CSVColumnInfo( "ColumnName", 666 ), "TestValue" );

            Assert.IsTrue( cell.Value == "TestValue" );
            Assert.IsTrue( cell.ColumnInfo.Name == "ColumnName" );
            Assert.IsTrue( cell.ColumnInfo.Index == 666 );
        }
    }
}
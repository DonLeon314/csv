﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSV
{
    public static class CSVFileUtils
    {
        public static void Add( this CSVFile destCSV, CSVFile source )
        {

            foreach( var new_row in source.Rows )
            {
                if( new_row.Count == 0 )
                {
                    continue;
                }

                Int32 idx_blank_row = destCSV.AppendBlank();

                foreach( var new_cell in new_row )
                {
                    CSVColumnInfo dest_column = destCSV.GetColumnInfo( new_cell.ColumnInfo.Name );
                    if( dest_column == null )
                    {
                        // Add column
                        dest_column = destCSV.AddColumn( new_cell.ColumnInfo.Name );
                    }
                    destCSV[idx_blank_row, dest_column.Index] = new_cell.Value;
                }
            }
        }
    }
}

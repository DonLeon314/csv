﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSV
{
    public class CSVData
    {

        private static char[] _DefaultSeparators = new char[] { ';' };

        public static char[] DefaultSeparators
        {
            get
            {
                return _DefaultSeparators;
            }
        }

        private List<List<String>> _Rows;
        
        public CSVData()
        {
            this._Rows = new List<List<String>>();
        }

        /// <summary>
        /// Create from file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static CSVData FromFile( String fileName )
        {
            CSVData result = new CSVData();

            using( StreamReader reader = new StreamReader( fileName ) )
            {
                result.AddFrom( reader );
            }

            return result;
        }

        public void Save( String fileName, Char separator )
        {
            StringBuilder sb = new StringBuilder();

            using( StreamWriter stream_out = new StreamWriter( fileName ) )
            {
                foreach( var row in this._Rows )
                {
                    sb.Clear();
                    foreach( var value in row )
                    {
                        if( sb.Length > 0 )
                        {
                            sb.Append( separator );
                        }
                        sb.Append( value );
                    }
                    stream_out.WriteLine( sb.ToString() );
                }
                
            }
        }

        /// <summary>
        /// Add from stream
        /// </summary>
        /// <param name="stream"></param>
        public void AddFrom( StreamReader stream )
        {
            String line = String.Empty;

            while( ( line = stream.ReadLine() ) != null )
            {
                AddRow( line );
            }
        }
        
        /// <summary>
        /// Add raw data
        /// </summary>
        /// <param name="rawString"></param>
        /// <param name="separators"></param>
        public void AddRow( String rawString, char[] separators = null )
        {
            if( String.IsNullOrWhiteSpace( rawString ) )
            {
                return;
            }

            if( separators == null || separators.Length ==0 )
            {
                separators = DefaultSeparators;                
            }

            String[] tokens = rawString.Split( separators );

            AddRow( tokens );
        }

        /// <summary>
        /// Add row data
        /// </summary>
        /// <param name="rowData"></param>
        public void AddRow( IEnumerable<String> rowData )
        {
            List<String> new_row = new List<String>( rowData );

            if( this._Rows.Count != 0 && this._Rows[0].Count < new_row.Count )
            {
                foreach( var row in this._Rows )
                {
                    for( Int32 i = row.Count; i < new_row.Count; i++ )
                    {
                        row.Add( "" );
                    }
                }
            }

            this._Rows.Add( new_row );
        }

        public Int32 AppendBlank( Int32 columns = 0 )
        {
            List<String> blank_row = new List<String>();

            Int32 size = ( columns == 0 ? ( this._Rows.Count> 0 ? this._Rows[0].Count : 0 ) : columns );

            for( Int32 i = 0; i < size; i++ )
            {
                blank_row.Add( "" );
            }

            this._Rows.Add( blank_row );

            return this._Rows.Count - 1;
        }

        /// <summary>
        /// Columns count
        /// </summary>
        public Int32 ColumnsCount
        {
            get
            {                
                return ( this._Rows.Count > 0 ? this._Rows[0].Count : 0 );
            }
        }

        /// <summary>
        /// Rows count
        /// </summary>
        public Int32 RowsCount
        {
            get
            {
                return ( this._Rows.Count > 0 ? this._Rows.Count : 0 );
            }
        }

        /// <summary>
        /// Get cell data
        /// </summary>
        /// <param name="row"> 0 base row index</param>
        /// <param name="column">0 base column index</param>
        /// <returns></returns>
        public String this[Int32 row, Int32 column]
        {
            get
            {
                if( row > ( this.RowsCount - 1 ) || column > ( this.ColumnsCount - 1 ) || row < 0 || column < 0 )
                {
                    throw new IndexOutOfRangeException();
                }

                return this._Rows[row][column];
            }

            set
            {
                if( row > ( this.RowsCount - 1 ) || column > ( this.ColumnsCount - 1 ) || row < 0 || column < 0 )
                {
                    throw new IndexOutOfRangeException();
                }

                this._Rows[row][column] = value;
            }
        }

        /// <summary>
        /// Column values
        /// </summary>
        /// <param name="columnIdx"></param>
        /// <returns></returns>
        public IEnumerable<String> ColumnValues( Int32 columnIdx )
        {
            foreach( List<String> row in this._Rows )
            {
                yield return row[columnIdx];
            }
        }

        public IEnumerable<List<String>> Rows
        {
            get
            {
                foreach( List<String> row in this._Rows )
                {
                    yield return row;
                }
            }
        }

    }
}

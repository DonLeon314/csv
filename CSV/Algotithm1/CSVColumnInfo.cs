﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSV
{
    /// <summary>
    /// CSV column Info
    /// </summary>
    public class CSVColumnInfo
    {
        public String Name
        {
            get;
            private set;
        }

        public Int32 Index
        {
            get;
            private set;
        }

        public CSVColumnInfo( String name, Int32 index )
        {
            this.Name = name;
            this.Index = index;
        }
    }
}

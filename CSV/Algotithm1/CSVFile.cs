﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSV
{
    public class CSVFile
    {        
        private List<String> _Header = new List<String>();        
        private CSVData _Data = new CSVData();

        public String File
        {
            get;
            private set;
        }

        public CSVFile( String fileName = null )
        {
            this.File = fileName;
        }

        public static CSVFile FromFile( String fileName, Boolean headerExist = true )
        {            
            using( StreamReader reader = new StreamReader( fileName ) )
            {
                CSVFile result = new CSVFile( fileName );
                
                String line = String.Empty;

                while( ( line = reader.ReadLine() ) != null )
                {
                    if( headerExist )
                    {
                        result.LoadHeader( line );
                        headerExist = false;
                    }
                    else
                    {
                        result.AddRow( line );
                    }
                }
                
                result.File = fileName;

                return result;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rawHeader"></param>
        /// <returns></returns>
        private Int32 LoadHeader( String rawHeader )
        {            
            this._Header.Clear();

            if( String.IsNullOrWhiteSpace( rawHeader ) == false )
            {
                String[] tokens = rawHeader.Split( CSVData.DefaultSeparators );
                foreach( String column_name in tokens )
                {
                    this._Header.Add( column_name.Trim() );
                }                
            }

            return this._Header.Count;
        }

        /// <summary>
        /// Add column
        /// </summary>
        /// <param name="columnName"></param>        
        public CSVColumnInfo AddColumn( String columnName )
        {


            for( Int32 idx = 0; idx < this._Header.Count; idx++ )
            {
                if( String.Compare( this._Header[idx], columnName, true ) == 0 )
                {
                    return new CSVColumnInfo( this._Header[idx], idx );
                }
            }

            this._Header.Add( columnName.Trim() );
            foreach( var row in this._Data.Rows )
            {
                for( Int32 i = row.Count; i < this._Header.Count; i++ )
                {
                    row.Add( "" );
                }
            }
            return new CSVColumnInfo( columnName, this._Header.Count - 1 );
        }

        /// <summary>
        /// Columns
        /// </summary>
        public IEnumerable<CSVColumnInfo> Columns
        {
            get
            {
                Int32 index = 0;
                foreach( String column_name in this._Header.AsReadOnly() )
                {
                    yield return new CSVColumnInfo( column_name, index );
                    index++;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Int32 AppendBlank()
        {
            return this._Data.AppendBlank( this._Header.Count );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rawString"></param>
        /// <param name="separators"></param>
        /// <returns></returns>
        public Int32 AddRow( String rawString, char[] separators = null )
        {
            String[] tokens = rawString.Split( separators == null ? CSVData.DefaultSeparators : separators );
            if( tokens.Length > this._Header.Count )
            {
                for( Int32 i = this._Header.Count; i < tokens.Length; i++ )
                {
                    this._Header.Add( "" );
                }
            }
            this._Data.AddRow( tokens );
            
            return this._Data.RowsCount - 1;
        }

        public Int32 AddRow( IEnumerable<CSVCellInfo> newRow, Int32 rowIndex = -1 )
        {
            Int32 append_idx = ( rowIndex == -1 ? AppendBlank() : rowIndex );

            foreach( var cell_info in newRow )
            {
                CSVColumnInfo ci = GetColumnInfo( cell_info.ColumnInfo.Name );
                if( ci == null )
                {
                    ci = AddColumn( cell_info.ColumnInfo.Name );
                }

                this[append_idx, ci.Index] = cell_info.Value;
            }

            return append_idx;
        }

        /// <summary>
        /// Rows Count
        /// </summary>
        public Int32 RowsCount
        {
            get
            {
                return _Data.RowsCount;
            }
        }

        /// <summary>
        /// Get or Set  cell data
        /// </summary>
        /// <param name="row"> 0 base row index</param>
        /// <param name="column">0 base column index</param>
        /// <returns></returns>
        public String this[Int32 row, Int32 col]
        {
            get
            {         
                return this._Data[row,col];
            }

            set
            {
                this._Data[row,col] = value;
            }
        }

        /// <summary>
        /// Column values
        /// </summary>
        /// <param name="columnIdx"></param>
        /// <returns></returns>
        public IEnumerable<String> ColumnValues( Int32 columnIdx )
        {
            return this._Data.ColumnValues( columnIdx );
        }

        /// <summary>
        /// Row values
        /// </summary>
        public IEnumerable<List<CSVCellInfo>> Rows
        {
            get
            {
                List<CSVColumnInfo> columns = new List<CSVColumnInfo>( this.Columns );
                
                foreach( List<String> row in this._Data.Rows )
                {
                    List<CSVCellInfo> result = new List<CSVCellInfo>();
                    Int32 idx = 0;
                    foreach( String value in row )
                    {
                        result.Add( new CSVCellInfo( columns[idx], value ) );
                        idx++;
                    }
                    yield return result;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public CSVColumnInfo GetColumnInfo( String columnName )
        {
            CSVColumnInfo result = null;

            columnName = columnName.Trim();

            for( Int32 idx = 0; idx < this._Header.Count; idx++ )
            {
                if( 0 == String.Compare( this._Header[idx], columnName, true ) )
                {
                    result = new CSVColumnInfo( this._Header[idx], idx );
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Save to StreamWriter
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="saveHeader"></param>
        /// <param name="separator"></param>
        public void Save( StreamWriter writer, Boolean saveHeader, String separator )
        {
            if( String.IsNullOrWhiteSpace( separator ) )
            {
                separator = ";";
            }

            StringBuilder file_row = new StringBuilder();

            if( saveHeader )
            {   // Save header 
                Boolean is_first = true;
                foreach( String column_name in this._Header )
                {
                    if( is_first == false )
                    {
                        file_row.Append( separator );
                    }
                    file_row.Append( column_name );
                    is_first = false;

                }

                writer.WriteLine( file_row.ToString() );
            }

            // Save data
            foreach( var row in this.Rows )
            {
                file_row.Clear();
                Boolean is_first =  true;
                foreach( var value in row )
                {
                    if( is_first == false )
                    {
                        file_row.Append( separator );
                    }

                    file_row.Append( value.Value );
                    is_first = false;
                }

                writer.WriteLine( file_row.ToString() );
            }
        }

        /// <summary>
        /// Save to file
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="saveHeader"></param>
        /// <param name="separator"></param>
        public void Save(  String fileName = null, Boolean saveHeader = true, String separator = null )
        {
            if( fileName != null )
            {
                this.File = fileName;
            }

            if( String.IsNullOrWhiteSpace( this.File ) )
            {
                throw new ArgumentException( "File name invalid" );
            }

            using( StreamWriter writer = new StreamWriter( this.File ) )
            {
                Save( writer, saveHeader, separator );
            }
        }
    }
}

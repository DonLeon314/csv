﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;

namespace CSV.Algotithm1
{
    internal class Algorithm1:
            IAgorithmCSV
    {
        private CorrectondenciasStorage _Correspondencias = new CorrectondenciasStorage();
        public Config _Config;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="config"></param>
        public Algorithm1( Config config )
        {
            this._Config = config;
        }

        /// <summary>
        /// 
        /// </summary>
        public String AlgorithmInfo
        {
            get
            {
                return "Algorithm 1 ";
            }
        }

        /// <summary>
        /// Process
        /// </summary>
        /// <param name="config"></param>
        public void Process()
        {
            LoadCorrecpondencias();

            DirectoryInfo folder = new DirectoryInfo( this._Config.PathInput );
            foreach( FileInfo file_info in folder.GetFiles( "*.csv" ) )
            {

                CSVData error_data = new CSVData();
                CSVData processado_data = new CSVData();

                CSVFile output_csv = new CSVFile();

                CSVData input_data = CSVData.FromFile( file_info.FullName );

                CSVFile row_csv = new CSVFile();
                String last_time = String.Empty;

                foreach( var src_row in input_data.Rows )
                {                    
                    if( String.IsNullOrWhiteSpace( last_time ) == false && String.Compare( last_time, src_row[3], true ) != 0 )
                    {
                        output_csv.Add( row_csv );
                        row_csv = new CSVFile();
                        last_time = String.Empty;
                    }                    

                    Correspondentia corr = FindCorrepondencia( new String[] { src_row[0], src_row[1], src_row[2] } );

                    if( corr == null )
                    {  // to error csv
                        error_data.AddRow( src_row );
                    }
                    else
                    { // to output csv
                        List<CSVCellInfo> new_row = new List<CSVCellInfo>();
                        if( row_csv.RowsCount == 0 )
                        {
                            string[] dateformats = new string[] { "dd/MM/yyyy HH:mm", "dd/MM/yyyy H:mm", "dd/MM/yyyy H:mm:ss", "dd/MM/yyyy H:mm:ss" };
                            string date = DateTime.ParseExact( src_row[3], dateformats, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces ).ToString( "yyyy/MM/dd" );
                            string time = DateTime.ParseExact( src_row[3], dateformats, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces ).ToString( "HH:mm:ss" );
                            CSVColumnInfo date_ci = row_csv.AddColumn( "Date" );
                            CSVColumnInfo time_ci = row_csv.AddColumn( "TIme" );
                            new_row.Add( new CSVCellInfo( date_ci, date ) );
                            new_row.Add( new CSVCellInfo( time_ci, time ) );
                        }

                        CSVColumnInfo caudal_ci = row_csv.AddColumn( corr.Caudal );
                        String val = ( Double.Parse( src_row[5].Replace( ",", "." ) ) * corr.FactorDeMultiplication ).ToString().Replace( ",", "." );
                        new_row.Add( new CSVCellInfo( caudal_ci, val ) );

                        CSVColumnInfo countador_ci = row_csv.AddColumn( corr.Countador );                        
                        new_row.Add( new CSVCellInfo( countador_ci, src_row[4].Replace(",", ".") ) );

                        // Add to row_scv
                        row_csv.AddRow( new_row, ( row_csv.RowsCount == 0 ? -1 : 0 ) );

                        last_time = src_row[3];

                        processado_data.AddRow( src_row );
                    }                    
                }
                
                if( row_csv.RowsCount != 0 )
                {
                    output_csv.Add( row_csv );
                }

                error_data.Save( this._Config.PathOutput + @"\Descartados\" + file_info.Name, ';' );
                processado_data.Save( this._Config.PathOutput + @"\Procesados\" + file_info.Name, ';' );                                

                output_csv.Save( this._Config.PathOutput + @"\" + this._Config.NambreOregin + "_" + DateTime.UtcNow.ToString( "dd_MM_yy_HH_mm_ss" ) + ".csv" );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="columns"></param>
        /// <returns></returns>
        private Correspondentia FindCorrepondencia( String[] columns )
        {
            foreach( String corr_id in columns )
            {
                if( this._Correspondencias.Contains( corr_id ) )
                {
                    return this._Correspondencias[corr_id];
                }
            }

            return null;            
        }

        /// <summary>
        /// Load correspondencias
        /// </summary>
        private void LoadCorrecpondencias()
        {
            CSVFile corr_csv = CSVFile.FromFile( this._Config.PathCorrespondencia );

            foreach( var row in corr_csv.Rows )
            {
                if( this._Correspondencias.Contains( row[0].Value ) == false )
                {                    
                    this._Correspondencias.Add( new Correspondentia( row[0].Value, row[1].Value, row[2].Value, row[3].Value ) );
                }
                else
                {
                    Console.WriteLine( String.Format( "WARNING: Correspondencia {0} is duplicated!", row[0].Value ) );
                }
            }

        }
    }
}

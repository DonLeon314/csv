﻿
using System;

using CSV;


namespace CSVTest
{
    class Program
    {
        static void Main( string[] args )
        {

            String alg = ( args == null || args.Length == 0 ? "1" : args[0] );

            try
            {
                Config config = new Config();

                if( config.LoadFromFile() == false )
                {
                    return;
                }

                IAgorithmCSV i_alg = AlgorithmCreator.CreateAlgorithm( alg, config );

                if( i_alg != null )
                {
                    i_alg.Process();
                }                
            }
            catch( Exception ex )
            {
                Console.WriteLine( String.Format( "\nERROR:\nMessage:{0}\nStack:{1}", ex.Message, ex.StackTrace ) );
            }

            return;
        }
    }
}
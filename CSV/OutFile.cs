﻿using System;
using System.IO;

namespace CSV
{
    public class OutFile
    {
        String _Path;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        public OutFile( String path )
        {
            _Path = path;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Boolean InitializePath()
        {
            try
            {

                if( !System.IO.Directory.Exists( this._Path ) )
                {
                    System.IO.Directory.CreateDirectory( this._Path );
                }

                return true;

            }catch( Exception ex )
            {
                Console.WriteLine( String.Format( "ERROR initialize path '{0}':{1}" , this._Path , ex.Message ) );
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public Boolean InitializeFile( String fileName )
        {
            try
            {
                String fn = this._Path + @"\" + fileName;

                if( File.Exists( fn ) )
                {
                    File.Delete( fn );                   
                }

                return true;

            }catch( Exception ex )
            {
                Console.WriteLine( String.Format( @"ERROR initialize file '{0}/{1}':{2}" , this._Path, fileName, ex.Message ) );
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="textLine"></param>
        public void SaveLine( String fileName , String textLine )
        {

            File.AppendAllText( this._Path + @"\" + fileName , textLine + "\r\n" );

        }
    }
}

﻿using System;


namespace CSV
{
    internal interface IAgorithmCSV
    {
        String AlgorithmInfo
        {
            get;
        }

        void Process();
    }
}

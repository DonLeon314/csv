﻿using System;

namespace CSV
{
    internal class AlgorithmCreator
    {

        public static IAgorithmCSV CreateAlgorithm( String algorithmName, Config config )
        {
            switch( algorithmName )
            {
                case "1":

                    return new CSV.Algotithm1.Algorithm1( config );

                case "0":

                    return new CSV.AlgorithmZero.AlgorithmZero( config );
            }

            throw new Exception( String.Format( "Unknown ALGORITHM:{0}", algorithmName ) );
        }
    }
}

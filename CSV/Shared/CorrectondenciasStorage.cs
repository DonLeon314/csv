﻿using System;
using System.Collections.ObjectModel;

namespace CSV
{
    /// <summary>
    /// 
    /// </summary>
    public class CorrectondenciasStorage:
        KeyedCollection<String, Correspondentia>
    {
        /// <summary>
        /// 
        /// </summary>
        public CorrectondenciasStorage():
            base(StringComparer.CurrentCultureIgnoreCase)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override String GetKeyForItem( Correspondentia item )
        {
            
            return item.IdImeter;
        }
    }
}

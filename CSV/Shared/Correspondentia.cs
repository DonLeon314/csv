﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSV
{
    public class Correspondentia
    {
        public String IdImeter
        {
            get;
            private set;
        } 
        public String Countador
        {
            get;
            private set;
        } 
        
        public String Caudal
        {
            get;
            private set;
        } 
        
        public Int32 FactorDeMultiplication
        {
            get;
            private set;
        }

        public Correspondentia( String idImeter, String countador, String caudal, String factorDeMultiplication )
        {
            this.IdImeter = idImeter;
            this.Countador = countador;
            this.Caudal = caudal;
            this.FactorDeMultiplication = Int32.Parse( factorDeMultiplication );
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSV
{
    public class Config
    {
        public String PathCorrespondencia
        {
            get;
            private set;
        }

        public String PathInput
        {
            get;
            private set;
        }

        public String PathOutput
        {
            get;
            private set;
        }

        public String NambreOregin
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Config()
        {
        }

        public Boolean LoadFromFile()
        {
            // Nindden:
            // Esta es una muy mala idea!
            // Es necesario almacenar los parámetros en INI o en XML
            
            using( StreamReader readDirectorios = new StreamReader( "directorios.txt" ) )
            {

                Int32 line_num = 0;

                    while( !readDirectorios.EndOfStream )
                    {
                        String line = readDirectorios.ReadLine().Trim();
                        line_num++;

                        switch( line_num )
                        {
                            case 2:

                                this.PathCorrespondencia = line;
                                break;

                            case 4:

                                this.PathInput = line;
                                break;

                            case 6:

                                this.PathOutput = line;
                                break;
                        }
                    }
                    
                    Int32 c = 0;
                    using( StreamReader readConfig = new StreamReader( File.OpenRead( "config.txt" ) ) )
                    {
                        while( !readConfig.EndOfStream )
                        {
                            String line = readConfig.ReadLine();
                            c++;
                            if( c == 2 )
                            {
                                this.NambreOregin = line;
                            }
                        }
                    }


                    return true;
            }
            
            return false;
        }
    }
}

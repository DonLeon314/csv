﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;

namespace CSV.AlgorithmZero
{
    internal class AlgorithmZero:
          IAgorithmCSV
    {
        public Config _Config;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="config"></param>
        public AlgorithmZero( Config config )
        {
            this._Config = config;
        }

        /// <summary>
        /// 
        /// </summary>
        public String AlgorithmInfo
        {
            get
            {
                return "Algorithm Zero (OLD!) ";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Process()
        {
            // ----------------------------------- old code


            //Creamos el lector que leerá el fichero para saber la correspondencia entre identificadores y
            //estaciones, así como el diccionario que contendrá estos pares.

            Dictionary<string, string> correspondencia = new Dictionary<string, string>();
            Dictionary<string, string> correspondenciaCaudal = new Dictionary<string, string>();
            Dictionary<string, string> correspondenciaFactor = new Dictionary<string, string>();

            try
            {
                using( StreamReader readerCorrespondencia = new StreamReader( this._Config.PathCorrespondencia ) )
                {
                    //Rellenamos el diccionario con los pares según la estación que corresponda.
                    string line;
                    string[] values;

                    while( !readerCorrespondencia.EndOfStream )
                    {
                        line = readerCorrespondencia.ReadLine();
                        values = line.Split( ';' );
                        if( values[0].Contains( "nombre" ) )
                            continue;
                        correspondencia.Add( values[0], "|" + values[1] + "|" );
                        correspondenciaCaudal.Add( values[0], "|" + values[2] + "|" );
                        correspondenciaFactor.Add( values[0], "|" + values[3] + "|" );
                    }
                }

            }
            catch( Exception ex )
            {
                Console.WriteLine( "No se ha podido abrir el fichero de correspondencia: {0}", ex.Message );
                Console.ReadKey();
                return;
            }

            //Identificamos el fichero de las lecturas del día de ayer, y lo abrimos con la creación del lector
            //que leerá los datos.

            //string lecturasHoy = Array.Find(Directory.GetFiles(@dirIn), x => x.Contains("Lecturas_" + DateTime.Now.AddDays(-1).ToString("yyyyMMdd")));


            //Mindden: 12 mar 2018 
            OutFile processado_file = new OutFile( this._Config.PathInput + @"\Procesados" );
            OutFile descartado_file = new OutFile( this._Config.PathInput + @"\Descartados" );
            if( processado_file.InitializePath() == false && descartado_file.InitializePath() == false )
            {
                return;
            }

            string[] ficheros = Directory.GetFiles( this._Config.PathInput );

            foreach( string fichero in ficheros )
            {

                string fileName = System.IO.Path.GetFileName( fichero );

                if( processado_file.InitializeFile( fileName ) == false && descartado_file.InitializeFile( fileName ) == false )
                {
                    return;
                }

                try
                {
                    using( StreamReader readerDatos = new StreamReader( fichero ) )
                    {
                        //Creamos los elementos necesarios para el tratamiento del fichero de las lecturas.
                        string linea;
                        string[] valores;
                        string valor;
                        List<string>[] arrayDeListas;
                        arrayDeListas = new List<string>[correspondencia.Count];
                        for( int i = 0; i < arrayDeListas.Length; i++ )
                        {
                            arrayDeListas[i] = new List<string>();
                        }
                        List<string> compr = new List<string>();
                        int pos = -1;

                        //Leemos el fichero y guardamos los datos de cada estación en su lista correspondiente 
                        //dándole ya el formato correcto a las fechas.              

                        while( !readerDatos.EndOfStream )
                        {
                            linea = readerDatos.ReadLine();
                            valores = linea.Split( ';' );

                            bool found = correspondencia.TryGetValue( valores[0], out valor );

                            if( ( found ) && ( !compr.Contains( valor ) ) )
                            {
                                pos++;
                                compr.Add( valor );
                                valor = valor.Remove( valor.Length - 1 );
                                arrayDeListas[pos].Add( this._Config.NambreOregin + "|1|Server Local|10|2" );
                            }

                            if( found )
                            {
                                string factor = correspondenciaFactor[valores[0]];
                                string[] FacDouble = factor.Split( '|' );
                                double FactorDouble = Double.Parse( FacDouble[1] );
                                string[] dateformats = new string[] { "dd/MM/yyyy HH:mm", "dd/MM/yyyy H:mm", "dd/MM/yyyy H:mm:ss", "dd/MM/yyyy H:mm:ss" };
                                string fecha = DateTime.ParseExact( valores[6], dateformats, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces ).ToString( "yyyy/MM/dd" );
                                string hora = DateTime.ParseExact( valores[6], dateformats, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces ).ToString( "HH:mm:ss" );
                                var sep = fecha.Split( ' ' );
                                fecha = sep[0];
                                arrayDeListas[pos].Add( correspondencia[valores[0]] + "0|" + fecha + "|" + hora + "|0|" + valores[4].Replace( ",", "." ) + "|192" );
                                arrayDeListas[pos].Add( correspondenciaCaudal[valores[0]] + "0|" + fecha + "|" + hora + "|0|" + ( Double.Parse( valores[5] ) * FactorDouble ).ToString().Replace( ",", "." ) + "|192" );
                            }

                            //Mindden: 12 mar 2018 

                            ( found ? processado_file : descartado_file ).SaveLine( fileName, linea );

                        }

                        //Creamos el string builder que volcará cada lista en su fichero correspondiente, ordenamos
                        //las listas, e iniciamos el proceso de volcado, se generarán tantos ficheros .csv como estaciones
                        //aparecen en la lectura y estén contenidas en la correspondencia.
                        string nombreFichero = this._Config.NambreOregin;


                        for( int i = 0; i < arrayDeListas.Length; i++ )
                        {
                            StringBuilder csv = new StringBuilder();
                            csv.AppendLine( "ASCII" );
                            csv.AppendLine( "|" );
                            if( arrayDeListas[i].Count > 0 )
                            {
                                arrayDeListas[i].Reverse();
                                arrayDeListas[i].Insert( 0, arrayDeListas[i][arrayDeListas[i].Count - 1] );
                                arrayDeListas[i].RemoveAt( arrayDeListas[i].Count - 1 );
                                for( int j = 0; j < arrayDeListas[i].Count; j++ )
                                {
                                    if( j == 0 )
                                    {
                                        string lineaCabecera = arrayDeListas[i][j];
                                        /* if (!lineaCabecera.Contains("Server")) {
                                             nombreFichero = lineaCabecera.Split('|')[3] + '_';
                                         }*/
                                    }
                                    csv.AppendLine( arrayDeListas[i][j].Trim( '|' ) );
                                }

                                string outputHoy = this._Config.PathOutput + "\\" + nombreFichero + "_" + DateTime.UtcNow.ToString( "dd_MM_yy_HH_mm_ss" ) + ".csv";
                                File.WriteAllText( outputHoy, csv.ToString() );
                                csv.Clear();
                                Thread.Sleep( 1000 );
                            }
                        }

                    }
                    // Delete file siurce
                    System.IO.File.Delete( fichero );

                }
                catch( Exception ex )
                {
                    Console.WriteLine( String.Format( "Error:{0}", ex.Message ) );
                    return;
                }
            }
        }
    }
}
